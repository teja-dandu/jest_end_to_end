// let loginAccount = require( '../actions/loginAccount' );

jest.setTimeout(100000)

describe('Basic authentication e2e tests', () => {
  beforeAll( async () => {
  // Set a definite size for the page viewport so view is consistent across browsers
    await page.setViewport( {
      width: 1366,
      height: 768,
      deviceScaleFactor: 1
    } );
     // loginAccount = await loginAccount( page );


    } );

  it( 'Should be truthy', async () => {
    expect( true ).toBeTruthy();
  })

  it( 'Should sign up page testing', async () => {
      try {
        page.on( 'dialog', dialog => {
          expect( dialog.message() ).toBe( 'Invalid username or password inputted or phonenumber');
          dialog.accept();
        });

        await page.goto( 'https://stagingapp3.tricaequity.com/login' );
        await page.click('.sign_up_link');
        await page.waitFor(5000) //Wait for the dialog to accept the prompt before proceeding
        await page.type('#firstName', 'teja');
        await page.type('#lastName', 'dandu');
        await page.type('#email', 'teja.dandu+111021@trica.co');
        await page.type('input[type="tel"]', '8500268905');
        await page.type('#brandName', 'trica');
        await page.waitFor(5000);
        await page.click('button[type = "submit"]');

        let successPanel = await page.waitForSelector('div.MuiGrid-root jss76 MuiGrid-item');
        expect(successPanel).toBeDefined();
        await browser.close()


      } catch(err){
        // console.log("An error occured while trying to signup => ", err)
        expect(err).toBeDefined();
      }
    })

  // it( 'Should sign up page testing', async () => {
  //     try {
  //       page.on( 'dialog', dialog => {
  //         expect( dialog.message() ).toBe( 'Invalid username or password inputted or phonenumber');
  //         dialog.accept();
  //       });

  //       await page.goto( 'https://stagingapp3.tricaequity.com/login' );
  //       await page.click('.sign_up_link');
  //       await page.waitFor(5000) //Wait for the dialog to accept the prompt before proceeding
  //       await page.type('#firstName', 'teja');
  //       await page.type('#lastName', 'dandu');
  //       await page.type('#email', 'teja.dandu+114@trica.co');
  //       await page.type('input[type="tel"]', '8500268905');
  //       await page.type('#brandName', 'trica');
  //       await page.waitFor(5000);
  //       await page.click('button[type = "submit"]');




  //     } catch(err){
  //       console.log("An error occured while trying to signup => ", err)
  //     }
  //   })



});
