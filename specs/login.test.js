
jest.setTimeout(100000)

describe('Basic authentication e2e tests', () => {
  beforeAll( async () => {
  // Set a definite size for the page viewport so view is consistent across browsers
    await page.setViewport( {
      width: 1366,
      height: 768,
      deviceScaleFactor: 1
    } );
     // loginAccount = await loginAccount( page );


    } );

  it( 'Should be truthy', async () => {
    expect( true ).toBeTruthy();
  })

  it( 'login page testing', async () => {
      try {
        page.on( 'dialog', dialog => {
          expect( dialog.message() ).toBe( 'Invalid username or password inputted or phonenumber');
          dialog.accept();
        });

        await page.goto('https://stagingapp3.tricaequity.com/login');
        await page.click('input#email');
        await page.waitFor(5000);
        await page.type('input#email', 'teja.dandu+02@trica.co')
        await page.click('input#email');
        await page.click('input#password');
        await page.waitFor(5000);
        await page.type('input#password', 'Teja@1364');
        await page.click('input#password');
        const signin_button = await page.waitForSelector('[class="btn esop-button primary signin_button"]');
        await page.waitFor(5000);
        await signin_button.click();

        const successPanel = await page.waitForSelector('div.MuiBox-root jss94 jss89');
        await page.waitFor(5000);
        expect(successPanel).toBeDefined();
        await browser.close();

      } catch(err){
        // console.log("An error occured while trying to signup => ", err)
        expect(err).toBeDefined();
      }
    })

    it( 'Invalid email page testing', async () => {
        try {
          page.on( 'dialog', dialog => {
            expect( dialog.message() ).toBe( 'Invalid username or password inputted or phonenumber');
            dialog.accept();
          });

          await page.goto('https://stagingapp3.tricaequity.com/login');
          await page.click('input#email');
          await page.waitFor(5000);
          await page.type('input#email', ' ')
          await page.click('input#email');
          await page.click('input#password');
          await page.waitFor(5000);
          await page.type('input#password', 'Teja@1364');
          await page.click('input#password');
          const signin_button = await page.waitForSelector('[class="btn esop-button primary signin_button"]');
          await page.waitFor(5000);
          await signin_button.click();

          const invalidInput = await page.$eval('input.invalid', (input) => input);
          expect(invalidInput).toBeUnDefined();
          // await browser.close();

        } catch(err){
          // console.log("An error occured while trying to signup => ", err)
          expect(err).toBeDefined();
        }
      })

    it( 'login invalid password given', async () => {
        try {
          page.on( 'dialog', dialog => {
            expect( dialog.message() ).toBe( 'Invalid username or password inputted or phonenumber');
            dialog.accept();
          });

          await page.goto('https://stagingapp3.tricaequity.com/login');
          await page.click('input#email');
          await page.waitFor(5000);
          await page.type('input#email', 'teja.dandu+02@trica.co')
          await page.click('input#email');
          await page.click('input#password');
          await page.waitFor(5000);
          await page.type('input#password', 'Teja@');
          await page.click('input#password');
          const signin_button = await page.waitForSelector('[class="btn esop-button primary signin_button"]');
          await page.waitFor(5000);
          await signin_button.click();

          const invalidInput = await page.$eval('input.invalid', (input) => input);
          expect(invalidInput).toBeDefined();
          // await browser.close();

        } catch(err){
          // console.log("An error occured while trying to signup => ", err)
          expect(err).toBe(true);
        }
      })


});
